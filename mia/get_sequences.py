import requests, sys
import argparse

def get_seq_sensembl(chr, start, end, ref=37, strand=1):
    """
    Get fasta sequence from ENSEMBL web service.
    :param chr: chromosome (character)
    :param start: start position (integer)
    :param end: end position (integer)
    :param ref: reference genome (character)
    :param strand: forward or reverse strand (integer)
    :return:
    """
    server = "http://grch{r}.rest.ensembl.org".format(r=str(ref))
    ext = "/sequence/region/human/{c}:{s}..{e}:{t}?".format(c=chr, s=str(start), e=str(end), t=strand)
    r = requests.get(server+ext, headers={ "Content-Type" : "text/plain"})    #x-fasta, plain
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    else:
        return r.text

def get_coords(coords):
    """
    Get coordinates in tab separated format.
    :param coords:
    :return:
    """
    #chr, coords = coords.rstrip().split(':')
    #start, end = map(int, coords.split('-'))
    chr, start, end = coords.split('\t')
    start, end = map(int, [start, end])
    return chr, start, end


def main(args):
    with open(args.output, 'w') as out:
        with open(args.input, 'r') as input:
            for l in input:
                chr, start, end = get_coords(l)
                res = get_seq_sensembl(chr, start, end)
                out.write(res)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Coordinates')
    parser.add_argument('-r', '--reference', required=False, default=37, help='Reference sequence.')
    parser.add_argument('-i', '--input', required=True, help='Input file.')
    parser.add_argument('-o', '--output', required=True, help='Output file.')
    args = parser.parse_args()
    main(args)