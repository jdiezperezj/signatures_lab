"""
Requieres: Python 2.7
This script returns takes the chrosomosmes/contigs of a given genome a assemble and returns a bed file that
contains the regions that couldn't be properly sequenced.
Output BED file format:
- chrom
- start (0 based)
- end (1 based)
- name (chr:start-end), 1 based
- length (end - start)

"""

import re
from itertools import groupby
import os


def fasta_iter(fasta_name):
    """
    Given a fasta file. yield tuples of header, sequence.
    :arg fasta_name: fasta file path.
    :returns: generator tuple(header, seq)
    """
    fh = open(fasta_name)
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        header = header.next()[1:].strip()
        seq = "".join(s.strip() for s in faiter.next())
        yield header, seq


def get_complex(genome, field_sep='\t'):
    """
    Get not unsequenced and/or masked regions in the reference genome.
    :arg genome: path to fasta file
    :returns: generator
    """
    rg = re.compile('N{1,}')
    iterator = fasta_iter(genome)
    for i in iterator:
        header, seq = i
        chrom = header.split()[0]   # keeps only chromsome name for that particular format.
        for j in rg.finditer(seq):
            yield field_sep.join(map(str, [chrom, j.start(), j.end() + 1,
                                      "{0}:{1}-{2}".format(chrom, j.start() + 1, j.end() + 1), j.end() - j.start()]))


def main(args, line_sep='\n'):
    """
    Write not solved regions in the human genome to a bed format file.
    :arg args: write to args.output file.
    """
    assert os.path.isfile(args.input), "File: {f} doen't exist or can't be read.".format(f=args.input)
    with open(args.output, 'w') as foh:
        for line in get_complex(args.input):
            foh.write(line + line_sep)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Get not solved regions in human genome.')
    parser.add_argument('-i', '--input', required=False,
                        default="/lustre/scratch110/srpipe/references/Homo_sapiens/1000Genomes_hs37d5/all/fasta/hs37d5.fa",
                        help='Input fasta file.')
    parser.add_argument('-o', '--output', required=False, default="unsolved.bed",
                        help="Output bed file. Writes to cwd 'unsolved.bed' if argument is not provided.")
    args = parser.parse_args()
    main(args)