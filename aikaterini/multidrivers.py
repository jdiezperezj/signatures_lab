#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
The formula we are using in order to define the drivers is the following

IF gene = Cancer Gene (match in the cosmic file and the variant file) AND Recessive
AND truncating mutation ("ess splice" or "nonsense" or ”frameshift” from the "Effect" column), then Driver

OR

IF gene = Cancer Gene AND Dominant AND ("missense", "inframe") mutation in multi driver list, then Driver.

For genes which are ambiguous and we do not know whether they are Dom or Rec

IF gene = Cancer Gene (from cosmic file and variant file)
AND truncating mutation ("ess splice" or "nonsense" or "frameshift" ), then Driver

AND

IF gene = Cancer Gene AND ("missense", "inframe") mutation in multi driver list, then Driver
"""

import argparse
from collections import namedtuple
import os
import sys


class Annotation(object):
    """
    Mother class for Cosmic and Multidrivers.
    """

    def get_lines(self, sep='\t'):
        with open(self.filename) as f:
            for n, line in enumerate(f.readlines()):
                if n != 0:
                    line = line.split(sep)
                    yield self.row(*line)



class Cosmic(Annotation):
    """
    Class to deal with COSMIC annotation files.
    """

    header = 'Gene_Symbol, Name, Entrez_GeneId, Genome_Location, Chr_Band, Somatic, Germline, TTSomatic, TTGermline, \
            Cancer_Syndrome, Molecular_Genetics, Mutation_Types'

    row = namedtuple('row', header)

    def __init__(self, filename):
        self.cols = self.header.split(', ')
        self.filename = filename
        self.records = tuple([r for r in self.get_lines()])
        self.dominants = tuple(sorted(set([r.Gene_Symbol for r in self.records if r.Molecular_Genetics == 'Dom'])))
        self.recessives = tuple(sorted(set([r.Gene_Symbol for r in self.records if r.Molecular_Genetics == 'Rec'])))
        self.ambiguous = tuple(sorted(set([r.Gene_Symbol for r in self.records if r.Molecular_Genetics
                                           not in ('Dom', 'Rec')])))

    def in_dominants(self, gene):
        return gene in self.dominants

    def in_recessives(self, gene):
        return gene in self.recessives

    def in_ambiguous(self, gene):
        return gene in self.ambiguous


class Multidrivers(Annotation):
    """
    Class to deal with MULTIDRIVERS' annotation file.
    """

    header = 'COSMIC_ALL, COSMIC_SOMATIC, INH_PATTERN, Coordinate_GRCh37, Identifier, Gene, Transcript, Chr, Pos, Type, \
    Class, cDNA, Protein, Institute, Study, Paper, bladder_urothelial_carcinoma_98, breast_adenocarcinoma_763, \
    colon_and_rectal_carcinoma_193, glioblastoma_multiforme_290, head_and_neck_squamous_cell_carcinoma_301, \
    kidney_renal_clear_cell_carcinoma_417, acute_myeloid_leukaemia_200, lung_adenocarcinoma_228, \
    lung_squamous_cell_carcinoma_174, ovarian_serous_carcinoma_316,	uterine_corpus_endometrial_carcinoma_230, sum, count'

    row = namedtuple('row', header)

    def __init__(self, filename):
        self.cols = self.header.split(', ')
        self.filename = filename
        self.records = tuple([r for r in self.get_lines()])
        self.dominants = tuple(sorted(set([r.Gene for r in self.records if r.INH_PATTERN == 'Dominant'])))
        self.recessives = tuple(sorted(set([r.Gene for r in self.records if r.INH_PATTERN == 'Recessive'])))
        self.ambiguous = tuple(sorted(set([r.Gene for r in self.records if r.INH_PATTERN
                                           not in ('Dominant', 'Recessive')])))

    def in_dominants(self, gene):
        """
        Check if gene name is dominant
        :param gene: string, gene name from variant file
        :return: boolean
        """
        return gene in self.dominants

    def in_recessives(self, gene):
        """
        Check if gene name is recessive.
        :param gene: string, gene name from variant file
        :return: boolean
        """
        return gene in self.recessives

    def in_ambiguous(self, gene):
        """
        Check if gene name is not clearly dominant or recessive.
        :param gene: string, gene name from variant file
        :return: boolean
        """
        return gene in self.ambiguous


class Variant(object):
    """
    Proxi class to analyse variant records easier.
    """
    def __init__(self, variant):
        self.nt = variant
        self.gene = self.nt.Gene

    def __repr__(self):
        return self.gene

    @property
    def is_truncating(self):
        return self.nt.Effect in ("ess splice", "nonsense", "frameshift")

    @property
    def is_minf(self):
        return self.nt.Effect in ("missense", "inframe")

    @property
    def is_missense(self):
        return self.nt.Effect == 'missense'

    @property
    def is_inframe(self):
        return self.nt.Effect == 'inframe'

    @property
    def is_frameshift(self):
        return self.nt.Effect == 'frameshift'


class Variants(object):

    def __init__(self, filename, sep='\t'):
        self.filename = filename
        self._ifh = open(self.filename, 'r')
        self.sep = sep
        line = self._ifh.readline()
        line = line.rstrip().split(self.sep)
        self.cols = ", ".join([f for f in self.rename(line)])
        self.header = self.cols.split(', ')
        self.row = namedtuple('row', self.cols)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._ifh.close()

    def __iter__(self):
        return self

    def next(self):
        line = self._ifh.readline()
        if line:
            return self.process_line(line)
        else:
            raise StopIteration

    def rename(self, fields):
        """
        Rename header's fields
        :param fields: list, each of the header's columns.
        :return: list, incorrect characters replaced.
        """
        for f in fields:
            try:
                int(f[0])
            except Exception:
                pass
            else:
                f = 'X_' + str(f)
            yield f.replace('.', '_')

    def process_line(self, line):
        """
        Process Variant file line
        :param line: string, transformed into list.
        :return: namedtuple, eache line element is named according to header.
        """
        line = line.rstrip().split(self.sep)
        assert len(line) == len(self.header), "Incorrect number of cols."
        row = self.row(*line)
        return Variant(row)


class FilterVariants(object):
    def __init__(self, variants, cosmic, multidrivers, output, sep='\t', eol='\n'):
        """
        Filter variant according to COSMIC and MULTIDRIVER annotation files.
        :param variants: Variant file.
        :param cosmic: cosmic reference file.
        :param multidrivers: multidrivers reference file.
        :param output: filtered output file.
        :param sep: string, separator character for output line
        :param eol: string, end of line for output file.
        :return: None, write to output file when called.
        """
        self._check_inputfiles(variants, cosmic, multidrivers, os.path.dirname(output))
        self.variants = Variants(variants)
        self.cosmic = Cosmic(cosmic)  # List of Gene ids.
        self.multidrivers = Multidrivers(multidrivers)    # List of gene ids.
        self.ofh = open(output, 'w')
        self.sep = sep
        self.eol = eol

    def __enter__(self):
        sys.stderr.write("Variant filtering started.\n")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ofh.close()
        sys.stderr.write("Variant filtering finished.\n")

    def _check_inputfiles(self, *files):
        for i in files:
            if not any([os.path.isfile(i), os.path.isdir(i)]):
                raise IOError, "Incorrect input path: {i}\n".format(i=i)

    def write(self, values):
        line = self.sep.join(values) + self.eol
        self.ofh.write(line)

    def __call__(self):
        for n, variant in enumerate(self.variants):
            if n == 0:
                self.write(variant.nt._asdict().keys() + ['drivers', ])
            res = self.filter(variant)
            if res[0]:
                self.write(variant.nt._asdict().values() + [res[1],])

    def filter(self, variant):
        if any([self.filter_cosmic(variant), self.filter_multidrivers(variant),
                self.filter_alternate1(variant), self.filter_alternate2(variant)]):
            return variant, 'driver'
        else:
            return variant, 'not-driver'

    def filter_cosmic(self, variant):
        """
        Filter truncating variants in recessive genes using COSMIC's gene list.
        :param variant: variant object instance
        :return: boolean
        """
        return True if self.cosmic.in_recessives(variant.gene) and variant.is_truncating else None

    def filter_alternate1(self, variant):
        """
        Filter truncating variants in ambigous Inh Pattern genes using COSMIC's gene list.
        :param variant: variant object instance
        :return: boolean
        """
        return True if self.cosmic.in_ambiguous(variant.gene) and variant.is_truncating else None

    def filter_multidrivers(self, variant):
        """
        Filter missense and in-frame variants in ambigous dominant genes using MULTIDRIVERS' gene list.
        :param variant: variant object instance
        :return: boolean
        """
        return True if self.multidrivers.in_dominants(variant.gene) and variant.is_minf else None

    def filter_alternate2(self, variant):
        """
        Filter missense and in-frame variants in ambigous Inh Pattern genes using MULTIDRIVERS' gene list.
        :param variant: variant object instance
        :return: boolean
        """
        return True if self.multidrivers.in_ambiguous(variant.gene) and variant.is_minf else None


def main(args):
    # If files are kept as inside the project:
    # cosmic_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), args.cosmic)
    # multidrivers_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), args.multidrivers)
    sys.stderr.write("Script execution started.\n")
    with FilterVariants(args.variants, args.cosmic, args.multidrivers, args.output) as variants:
        variants()
    sys.stderr.write("Script execution finished.\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Famous multidriver parser')
    parser.add_argument('-v', '--variants', required=True, action='store', help='Variants file.')
    parser.add_argument('-c', '--cosmic', required=False,
                        default='data/cosmic_f.txt',
                        action='store', help='COSMIC file.')
    parser.add_argument('-m', '--multidrivers', required=False,
                        default='data/Multi_Study_Drivers_25_11_2015_V1_f.txt',
                        action='store',  help='Multidrivers.')
    parser.add_argument('-o', '--output', required=True, action='store', help='Output file.')
    args = parser.parse_args()
    main(args)